Rails.application.routes.draw do

  root 'pages#home'
  get '/home', to: 'pages#home'

  # get '/recepies', to: 'recepies#index'
  # get '/recepies/new', to: 'recepies#new', as: 'new_recepie'
  # post '/recepies', to: 'recepies#create'
  # get  '/recepies/:id/edit', to: 'recepies#edit', as: 'edit_recepie'
  # patch '/recepies/:id', to: 'recepies#update'
  # get '/recepies/:id', to: 'recepies#show', as: 'recepie'
  # delete '/recepies/:id', to:'recepies#destroy'

  resources :recipes


end