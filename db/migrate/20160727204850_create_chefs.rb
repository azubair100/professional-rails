class CreateChefs < ActiveRecord::Migration
  def change
    create_table :chefs do |t|
      t.string :chef_name, null:false, limit:100
      t.string :email, null:false, limit:100

      t.timestamps null: false
    end
  end
end
