class CreateRecepies < ActiveRecord::Migration
  def change
    create_table :recipes do |t|
      t.string :name, limit: 100, null: false
      t.string :summary, limit: 200
      t.text :description
      t.integer :chef_id

      t.timestamps null: false
    end
    add_index :recipes, :chef_id

  end
end
